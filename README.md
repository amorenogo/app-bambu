# README

### How to install

---

- Open a prompt and type `git@gitlab.com:amorenogo/app-bambu.git`
- Install dependencies `npm install`

### How to start

---

- Start on **development mode:** `npm run start`
- Open [`http://localhost:3000`](http://localhost:3000) to view it in the browser

### About

---

- **Alternativas Sostenibles**: Conoce varias alternativas ecológicas y sostenibles para sustituir a los productos de siempre de usar y tirar
- **Comercio Local:** Descubre las tiendas de tu barrio que estén concienciadas con el medio ambiente y que sean sostenibles

### Technical Specification 💻

---

- React
- Hooks
- Prop Types
- JS, HTML, CSS, SCSS, BEM Methodology
- Responsive Design
- Git
- Libraries:
    - Material UI
    - react-toastify
    - react-mapbox-gl
    - react-responsive-carousel

### Features 🍃

---

- User Authentication
- User Sign Up
- Form Validation
- User Profile
- Products Categories
- Products
- Product Detail
- Add products to Favorites
- View stores in map
- Display store info in map marker
- Global products search