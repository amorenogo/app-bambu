import React, { useContext, useState } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

// styles
import '../styles/Navbar.scss';

// icons
import SearchIcon from '@material-ui/icons/Search';
import ViewModuleIcon from '@material-ui/icons/ViewModule';
import PersonIcon from '@material-ui/icons/Person';
import RoomIcon from '@material-ui/icons/Room';

// context
import { ActiveUserContext } from '../context';

const Navbar = ({ products }) => {
    const [filter, setFilter] = useState('');
    const [activeUser] = useContext(ActiveUserContext);
    const handleChangeFilter = (ev) => {
        const value = ev.target.value;
        setFilter(value);
      }
      const filteredProducts = products.filter((product) => {
        const name = product.name.toLowerCase().trim();
        const filterInput = filter.toLowerCase().trim();
    
        return name.includes(filterInput);
      }) 
      
    return (
        <div className="Navbar">
            <Link to="/home">
                <img className="Navbar__logo" src="/assets/images/bambu-logo.svg" alt="bambú logo"/>
            </Link>
            <div className="Navbar__center">
                <input className="Navbar__search-input" type="text"  
                placeholder="Busca tu producto"
                value={filter}
                onChange={handleChangeFilter}/>
                <SearchIcon className="Navbar__search-icon"/>
                {filter && filteredProducts.length ?(
                    <div className="Navbar__search-result">
                        {filteredProducts.map((filteredProduct)=> {
                            return(
                                <Link key={filteredProduct.id} to={`/product/${filteredProduct.id}`} onClick={() => setFilter('')}>{filteredProduct.name}</Link>
                            )
                        })}
                    </div>) : null} 
            </div>
            <div className="Navbar__right">
                <Link  to="/categories">
                    <ViewModuleIcon className="Navbar__right--categories"/>
                </Link>
                <Link to="/stores">
                    <RoomIcon className="Navbar__map-icon"/>
                </Link>
                <Link to={ activeUser ? "/profile" : "/access" }>
                    <PersonIcon className="Navbar__right--access"/>
                </Link>
            </div>
        </div>
    )
}

export default Navbar;

Navbar.propTypes = {
    products: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.number,
        name: PropTypes.string,
        imageURL:PropTypes.string,
        price: PropTypes.number,
        shops: PropTypes.arrayOf(PropTypes.number),
        category: PropTypes.string,
        type: PropTypes.string,
        description: PropTypes.string,
        details: PropTypes.string,
        material: PropTypes.string,
        care: PropTypes.string,
        size: PropTypes.shape({
            width: PropTypes.number,
            height: PropTypes.number
        }),
    }))
}