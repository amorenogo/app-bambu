import React, { useContext } from 'react';
import { Carousel } from 'react-responsive-carousel';

// styles
import '../styles/CarouselCard.scss';
import "react-responsive-carousel/lib/styles/carousel.min.css";

// context
import { BambuInfoContext } from '../context';

const CarouselCard = () => {
    const [bambuInfo] = useContext(BambuInfoContext);

    return (
        <div className="CarouselCard">
            <Carousel >
                {bambuInfo.map((infoElement) => {
                    return(
                        <div className="CarouselCard__container" key={ infoElement.id }>
                            <img className="CarouselCard__img" src={ infoElement.image } alt={ infoElement.title }/>
                            <h3>{ infoElement.title }</h3>
                            <p>{ infoElement.description }</p>                    
                        </div>
                    )
                })}                
            </Carousel>
        </div>
    )
}

export default CarouselCard;
