import React from 'react';
import { Link } from 'react-router-dom';

// styles
import '../styles/Footer.scss';

const Footer = () => {
    return (
        <footer className="Footer">
            <div className="Footer__info-container">
                <p className="Footer__info">&copy; 2020 | Bambú</p>
            </div> 
            <Link className="Footer__about" to="/about">
                <p className="Footer__about-info">Sobre nosotras</p>
            </Link>  
        </footer>
    )
}

export default Footer;
