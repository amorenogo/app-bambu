import React from 'react';
import { Link } from 'react-router-dom';

// styles
import '../styles/About.scss';
import ImportantDevicesIcon from '@material-ui/icons/ImportantDevices';

export default function About({about}) {
    return (
        <div className="About">
            {about.map((aboutUs) => {
                return (
                    <div key={aboutUs.name} className="About__info">
                        <img className={aboutUs.type === "UX/UI Designer" ? "About__ux" : "About__img"} src={ aboutUs.imageURL } alt={ aboutUs.name }/>
                        <h3 className="About__name">{ aboutUs.name } { aboutUs.surname }</h3>
                        <p className="About__icon">{ aboutUs.type } <ImportantDevicesIcon className="Computer" /></p>
                        <div className="About__social">
                            <a href={ aboutUs.link } target="_blank">
                                <img className="About__link-img" src="/assets/images/linkedin.svg" alt="Linkedin"/>
                            </a>
                            <a href={ aboutUs.secondLink } target="_blank">                                
                                <img className="About__gitlab-img" 
                                    src={aboutUs.type === "UX/UI Designer" ? "/assets/images/behance.svg" : "/assets/images/gitlab.svg"}
                                    alt={aboutUs.type === "UX/UI Designer" ? "Behance" : "Gitlab"}/>
                            </a>
                        </div>
                        <p className="About__description">{ aboutUs.description }</p>
                    </div> 
                );   
            })}
        </div>
    )
}
