import React, {useState, useEffect} from 'react';
import PropTypes from 'prop-types';

//styles
import '../../styles/Access/UserLoginForm.scss';
import VisibilityIcon from '@material-ui/icons/Visibility';
import VisibilityOffIcon from '@material-ui/icons/VisibilityOff';

export default function UserLoginForm({checkExistingUser, isLoginValid}) {
    const [isShowingPassword, setIsShowingPassword] = useState(false);
    const [isDisabled, setIsDisabled] = useState(true);
    const [formValues, setFormValues] =  useState({
        email:'',
        password: '', 
    });
    
    useEffect(() => {
        if (isLoginValid){
            resetForm();
       }
    }, [isLoginValid])

    useEffect(() => {
        toggleDisabledAccess();
    }, [formValues])

    const handleOnSubmit = (ev) => {
        ev.preventDefault();
        checkExistingUser(formValues);
    };

    const handleOnChange = (ev) => {
        const name = ev.target.name;
        const value = ev.target.value;

        setFormValues({
            ...formValues,
            [name]:value,   
        })
    };

    const resetForm = () => {
        setFormValues({
            email: '',
            password: '',
        })
    };

    const toggleShowPassword = () => {
        setIsShowingPassword(!isShowingPassword);       
    }

    const toggleDisabledAccess = () => { 
       if(formValues.email === "" || formValues.password === ""){
        setIsDisabled(true); 
       }else{
        setIsDisabled(false)
       }       
    }

    return (
        <form className="UserLoginForm" onSubmit={handleOnSubmit}>
            <input className="UserLoginForm__email" type="email" placeholder="Dirección de correo electrónico" name="email" minLength="10" maxLength="50" required value={formValues.email} onChange={handleOnChange} />            
           <div className="UserLoginForm__password-container">
                <input className="UserLoginForm__password-input" type={isShowingPassword ? "text" : "password"} placeholder="Contraseña" name="password" value={formValues.password} required pattern="[A-Za-z0-9]{8,40}" onChange={handleOnChange} />
                {isShowingPassword ? <VisibilityOffIcon className="UserLoginForm__password-icon" onClick={toggleShowPassword}/> :
                    <VisibilityIcon className="UserLoginForm__password-icon" onClick={toggleShowPassword}/>
                    }
            </div>
            <div>
                <button className={isDisabled ? "UserLoginForm__button--disabled button-template" : "UserLoginForm__button button-template"} type="submit"  >Inicia sesión</button> 
            </div>
        </form>
    )
}



UserLoginForm.propTypes = {
    checkExistingUser: PropTypes.func,
    isLoginValid: PropTypes.bool,
}
