import React, { useState, useEffect, useContext } from 'react';
import { useLocation, useHistory } from 'react-router-dom';
import { toast } from 'react-toastify';
import { v4 as uuid } from 'uuid';

// context
import { UsersContext } from '../../context';
import { ActiveUserContext } from '../../context';

// styles
import '../../styles/Access/Access.scss';

// components
import UserLoginForm from './UserLoginForm';
import UserSignUpForm from './UserSignUpForm';

const Access = () => {
    const location = useLocation();
    const history = useHistory();
    const [activeRoute, setActiveRoute] = useState('');
    const [isLoginSelected, setIsLoginSelected] = useState(true); 
    const [isLoginValid, setIsLoginValid] = useState(false);
    const [isSignUpValid, setIsSignUpValid] = useState(false);
    const [users, setUsers] = useContext(UsersContext);
    const [activeUser, setActiveUser] = useContext(ActiveUserContext);   

    // updates active route when url changes
    useEffect(() => {
        setActiveRoute(location.pathname.slice(1));
    }, [activeRoute]);

    // go to profile when user logs in or registers
    useEffect(() => {
        if(activeUser) {
            history.push('/profile');
        }
    }, [activeUser]);

    // updates boolean to display or hide login or sign up form 
    const handleClick = (type) => {
        if(type === 'login') {
            setIsLoginSelected(true);
        }else if (type === 'sign-up') {
            setIsLoginSelected(false)
        };
    }

    // check if user exists and verifies password 
    const checkExistingUser = ({ email, password }) => {
        const activeUser = users.find((user) => user.email.toLowerCase() === email.toLowerCase())
        if(activeUser && activeUser.password === password) {
            toast.success('🍃 ¡Has accedido a tu cuenta! 🍃');
            setActiveUser(activeUser);
            setIsLoginValid(true);
        } else {
            setIsLoginValid(false);
        }
    }

    // create new user when signs up
    const createNewUser = ({ name, surname, email, password }) => {
        const newUser = {
            id: uuid(),
            name: name,
            surname: surname,
            email: email,
            password: password,
        }
        setUsers([
            ...users,
            newUser
        ]);
        toast.success('🍃 ¡Has creado una cuenta con éxito! 🍃');
        setActiveUser(newUser); 
        setIsSignUpValid(true);
    }

    return (
        <div className="Access">
            <div className="Access__buttons">
                <a className={ isLoginSelected ? "Access__active Access__button": "Access__button-login Access__button" } onClick={ () => handleClick('login')}>Acceder</a>
                <a className={ !isLoginSelected ? "Access__active": "Access__button-sign-up" } onClick={ () => handleClick('sign-up')}>Registarse</a>
            </div>
            { isLoginSelected ? 
                <UserLoginForm checkExistingUser={ checkExistingUser } isLoginValid={isLoginValid}/> 
                : <UserSignUpForm createNewUser={ createNewUser } isSignUpValid={ isSignUpValid }/> }
          
        </div>
    )
}

export default Access;