import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

// styles
import VisibilityIcon from '@material-ui/icons/Visibility';
import VisibilityOffIcon from '@material-ui/icons/VisibilityOff';
import '../../styles/Access/UserSignUpForm.scss';

const UserSignUpForm = ({ createNewUser, isSignUpValid }) => {
    const [isShowingPassword, setIsShowingPassword] = useState(false);
    const [formValues, setFormValues] =  useState({
        name: '',
        surname: '', 
        email: '', 
        password: '', 
    });
    const [isDisabled, setIsDisabled] = useState(true);

    useEffect(() => {
        if (isSignUpValid){
            resetForm();
       }
    }, [isSignUpValid])

    useEffect(() => {
        toggleDisabledAccess();
    }, [formValues])

    const handleOnSubmit = (ev) => {
        ev.preventDefault();
        createNewUser(formValues);
    };

    const handleOnChange = (ev) => {
        const name = ev.target.name;
        const value = ev.target.value;

        setFormValues({
            ...formValues,
            [name]:value,   
        });
    };

    const toggleShowPassword = () => {
        setIsShowingPassword(!isShowingPassword);       
    }

    const resetForm = () => {
        setFormValues({
            name: '',
            surname: '',
            email: '',
            password: '',
        })
    };

    const toggleDisabledAccess = () => { 
        if(formValues.name === "" || formValues.surname === "" || formValues.email === "" || formValues.password === ""){
         setIsDisabled(true); 
        }else{
         setIsDisabled(false)
        }       
     };

    return (
        <div className="UserSignUpForm">
            <form className="SignForm" onSubmit={ handleOnSubmit }>
                <input className="SignForm__name" type="text" placeholder="Nombre" name="name" minLength="2" required value={ formValues.name } onChange={ handleOnChange } />
                <input className="SignForm__surname" type="text" placeholder="Apellido" name="surname" minLength="2" required value={ formValues.surname } onChange={ handleOnChange } />
                <input className="SignForm__email" type="email" placeholder="Dirección de correo electrónico" name="email" minLength="10" maxLength="50" required value={ formValues.email } onChange={ handleOnChange } />
                <div className="SignForm__password-container">
                    <input className="SignForm__password" type={isShowingPassword ? "text" : "password"} placeholder="Contraseña" name="password" value={formValues.password} required pattern="[A-Za-z0-9]{8,40}" onChange={handleOnChange} />
                    { isShowingPassword ? <VisibilityOffIcon className="SignForm__password-icon" onClick={toggleShowPassword}/> : <VisibilityIcon className="SignForm__password-icon" onClick={toggleShowPassword}/> }
                </div>
                <p className="SignForm__terms">Al continuar, aceptas los Terminos y condiciones y Política de privacidad.</p>
                <button className={isDisabled ? "SignForm__button--disabled button-template" : "SignForm__button button-template"} type="submit">Regístrate</button> 
            </form> 
        </div>
    )
}

export default UserSignUpForm;

UserSignUpForm.propTypes = {
    createNewUser: PropTypes.func,
    isSignUpValid: PropTypes.bool,
}