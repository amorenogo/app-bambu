import React, { useState, useContext} from 'react';
import ReactMapboxGl, { Marker } from 'react-mapbox-gl';

// styles 
import '../styles/StoresMap.scss';

// context
import { StoresContext } from '../context';

// component
import MarkerPopUp from './MarkerPopUp';
 
const Map = ReactMapboxGl({
  accessToken:
    'pk.eyJ1IjoiYW1vcmVub2dvIiwiYSI6ImNrZm12aG9tZTIyY2MycW52Z2Z6aGR0OXMifQ.dqECL0T4oGqHQrl4dOGnNQ'
});


const StoresMap = () => {
    const [stores] = useContext(StoresContext);
    const [displayPopUp, setDisplayPopUp] = useState(false);
    const [selectedStore, setSelectedSotre] = useState();
    const [mapState, setMapState] = useState({
        lat: 40.414617,
        lng: -3.6957941,
        zoom: 15
    });

    const handleToogleDisplayPopup = (store) => {
        setSelectedSotre(store)
        setDisplayPopUp(!displayPopUp)
    }
  
    return (
        <div className="StoresMap">
                <Map
                style="mapbox://styles/mapbox/streets-v9"
                containerStyle={{
                    height: '77vh',
                    width: '100vw'
                }}
                center={[mapState.lng, mapState.lat]}
                zoom={[13]}>
                    {stores.map((store) => {
                        return (
                            <div key={ store.location.lng + store.name }>
                                <Marker                                    
                                    coordinates={[store.location.lng, store.location.lat]}
                                    anchor="bottom"
                                    onClick={ () => handleToogleDisplayPopup(store) }
                                    >
                                    <img className="StoresMap__marker" src="/assets/images/map-marker.svg" alt="marker"/>
                                </Marker>   
                            </div>
                        )
                    })}     
                    { displayPopUp ? 
                        <MarkerPopUp 
                            key={ selectedStore.id }
                            id={ selectedStore.id }
                            lng={ selectedStore.location.lng }
                            lat={ selectedStore.location.lat }
                            name={ selectedStore.name }
                            schedule={ selectedStore.schedule }
                        /> : null }     
                </Map>
                    <div className="StoresMap__stores-container">
                        {stores.map((store) => {
                            return(
                                <div className="StoresMap__store" key={ store.id }>
                                    <img src={ store.imageURL } alt={ store.name }/>
                                    <div className="StoresMap__store-info">
                                        <h4 className="StoresMap__store-name">{ store.name }</h4>
                                        <p className="StoresMap__store-description">{ store.description }</p>
                                        <p className="StoresMap__store-schedule">{ store.schedule }</p>
                                    </div>
                                </div>
                            )
                        })}
                    </div>    
        </div>
    )
}

export default StoresMap;
