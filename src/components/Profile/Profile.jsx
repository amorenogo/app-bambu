import React, { useContext, useState } from 'react';
import { Redirect } from 'react-router-dom';

// styles
import '../../styles/Profile/Profile.scss';

// icons
import AddIcon from '@material-ui/icons/Add';
import MyLocationIcon from '@material-ui/icons/MyLocation';
import WorkIcon from '@material-ui/icons/Work';

// context
import { ActiveUserContext, FavProductsContext } from '../../context';

// components
import ProductCard from '../Product/ProductCard';

const Profile = () => {
    const [activeUser] = useContext(ActiveUserContext);
    const [favProducts] = useContext(FavProductsContext);
    const [isDispayDetail, setIsDisplayDetail] = useState(false);

    if(!activeUser) {
        return <Redirect to="/access"/>;
    }

    return (
        <div className="Profile">            
            <h3 className="Profile__title">Mi perfil</h3>
            <div className="Profile__header">
                {activeUser.imageURL ? (
                        <img className="Profile__image" src={ activeUser.imageURL } alt={ activeUser.name }/>
                ) : null}
                <div className="Profile__main-info">
                    <h4 className="Profile__name">{ activeUser.name } { activeUser.surname }</h4>
                    <button className="Profile__detail-button button-template" onClick={ () => setIsDisplayDetail(!isDispayDetail)}>
                        {isDispayDetail ? 'Ocultar Detalle' : "Mostrar Detalle"}
                    </button> 
                </div>
            </div>
            <div className="Profile__second-container">
                <div className="Profile__second-item">
                    <MyLocationIcon className="Location"/>
                    <p className="Profile__second-location">{ activeUser.location }</p>
                </div>
                <div className="Profile__second-item Profile__second-item--job">
                    <WorkIcon className="Job"/>
                    <p className="Profile__second-job">{ activeUser.job }</p>
                </div>                
            </div>
            <div className="Product__title-container">
                <h4 className="Product__products-title">Productos</h4>
            </div>
            <div className="Profile__products">
                {favProducts.map((favProduct) => {
                        return(
                            <ProductCard 
                                key={ favProduct.id }
                                id={ favProduct.id }
                                imageURL={ favProduct.imageURL }
                                category={ favProduct.category }
                                name={ favProduct.name }
                                price={ favProduct.price }
                                activeRoute='product'
                            />
                        )
                    })}      
            </div>
            {isDispayDetail && activeUser ? (
                <div>
                    {activeUser.biography ? (
                        <div>
                            <h4>Descripción</h4>                        
                            {activeUser.biography.map((bio) => <p>{ bio }</p>)}                
                        </div>
                    ) : null}
                    {activeUser.motivations ? (
                        <div>
                            <h4>Motivaciones</h4>
                            <p>{ activeUser.motivations }</p>
                        </div>
                    ) : null}    
                    {activeUser.needs ? (
                        <div>
                            <h4>Necesidades</h4>
                            {activeUser.needs.map((need) => <p>{ need }</p>)}                
                        </div>
                    ) : null}    
                    {activeUser.frustrations ? (
                        <div>
                            <h4>Frustraciones</h4>
                            {activeUser.frustrations.map((frustration) => <p>{ frustration }</p>)}                
                        </div>
                    ) : null}        
                </div>
            ) : null } 
        </div>
    )
}

export default Profile;
