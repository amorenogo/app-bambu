import React from 'react';
import { Link } from 'react-router-dom';

// styles
import '../styles/LaunchScreen.scss';

const LaunchScreen = () => {
    return (
        <div className="LaunchScreen">
                <Link to="/home" className="LaunchScreen__link">
                    <button className="button-template LaunchScreen__btn">Explora</button>        
                </Link>                
        </div>
    )
}

export default LaunchScreen;
