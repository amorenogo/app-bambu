import React from 'react';
import { Popup } from 'react-mapbox-gl';
import PropTypes from 'prop-types';

// styles
import '../styles/MarkerPopUp.scss';

const MarkerPopUp = ({ id, lng, lat, name, schedule }) => {
    return (
        <div className="MarkerPopUp">
                <Popup
                    key={ lng + ',' + lat + 'popup'}
                    coordinates={[lng, lat]}
                    offset={{
                    'bottom-left': [12, -38],  'bottom': [0, -38], 'bottom-right': [-12, -38]
                    }}>
                    <h4 className="MarkerPopUp__name">{ name }</h4>
                    <p className="MarkerPopUp__schedule">{ schedule }</p>
                </Popup>            
        </div>
    )
}

export default MarkerPopUp;

MarkerPopUp.propTypes = {
    id: PropTypes.number,
    lng: PropTypes.number,
    lat: PropTypes.number,
    name: PropTypes.string,
    schedule: PropTypes.string,
}