import React from 'react';

// styles
import '../styles/Home.scss';

// components
import CarouselCard from './CarouselCard';

const Home = () => {
    return (
        <div className="Home">
            <section className="Home__carousel">
                <CarouselCard />
            </section>
        </div>
    )
}

export default Home;
