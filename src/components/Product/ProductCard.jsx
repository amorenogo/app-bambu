import React, { useState, useEffect, useContext } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

// styles
import '../../styles/Product/ProductCard.scss';

// icons
import FavoriteIcon from '@material-ui/icons/Favorite';
import FavoriteBorderIcon from '@material-ui/icons/FavoriteBorder';

// context
import { FavProductsContext, ProductsContext } from '../../context';

const ProductCard = ({ id, imageURL, type, name, price, activeRoute }) => {
    const [descriptionRoute, setDescriptionRoute] = useState('');
    const [products] = useContext(ProductsContext);
    const [favProducts, setFavProducts] = useContext(FavProductsContext);

    const isFav = Boolean(favProducts.find((favProduct) => favProduct.id == id));

    useEffect(() => {
        setDescriptionRoute(`/${ activeRoute }/${ id }`);
    }, [])    

    const toggleFav = () => {
        const product = products.find((product) => product.id == id);
        if(!isFav) {               
            setFavProducts([
                ...favProducts, 
                product
            ]);
        } else {
            const filteredFav = favProducts.filter((fav) => fav.id !== product.id)
            setFavProducts(filteredFav);
        }
    }
    
    return (
        <div className="ProductCard">
            <Link to={ descriptionRoute }>
                <img className="ProductCard__img" src={ imageURL } alt={ name }/>
                {type ? (
                    <p className="ProductCard__type">{ type.split("-").join(" ") }</p>
                ) : null}                
                <p className="ProductCard__name">{ name }</p>
                <p className="ProductCard__price">{ price }€</p>
            </Link>
            {isFav ? <FavoriteIcon className="ProductCard__favorite" onClick={ toggleFav }/> : <FavoriteBorderIcon className="ProductCard__favorite--border" onClick={ toggleFav }/>}                        
        </div>
    )
}

export default ProductCard;

ProductCard.propTypes = {
    id: PropTypes.number,
    imageUrl: PropTypes.string,
    type: PropTypes.string,
    name: PropTypes.string,
    price: PropTypes.number,
    activeRoute: PropTypes.string
}
