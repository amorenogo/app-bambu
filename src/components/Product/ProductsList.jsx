import React, { useEffect, useState } from 'react';
import { useHistory, useLocation } from 'react-router-dom';
import PropTypes from 'prop-types';

// styles
import '../../styles/Product/ProductsList.scss';

// icons
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';

// components
import ProductCard from './ProductCard';

const ProductsList = ({ filteredProducts, activeRoute }) => {
    const history = useHistory();
    const location = useLocation();
    const [title, setTitle] = useState('');

    useEffect(() => {
        const route = location.pathname.slice(1);
        switch(route) {
            case 'food':
                setTitle('Alimentos');
                break;
            case 'hygiene':
                setTitle('Higiene');
                break;
            case 'makeup':
                setTitle('Cosmética');
                break;
            case 'takeaway':
                setTitle('Para Llevar');
                break;
            default:
                break;
        }
    }, [])
    
    return (
        <div className="ProductsList">            
            <div className="ProductsList__header">
                <ArrowBackIosIcon className="ProductsList__go-back" onClick={ history.goBack }/>
                <h4 className="ProductsList__title">{ title }</h4>
            </div>
            <div className="ProductsList__products">
            {filteredProducts.map((filteredProduct) => {
                return(
                    <ProductCard 
                        key={ filteredProduct.id }
                        id={ filteredProduct.id }
                        imageURL={ filteredProduct.imageURL }
                        type={ filteredProduct.type }
                        name={ filteredProduct.name }
                        price={ filteredProduct.price }
                        activeRoute={ activeRoute }
                    />                    
                )
            })}
            </div>
        </div>
    )
}

export default ProductsList;

ProductsList.propTypes = {
    filteredProducts: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.number,
        name: PropTypes.string,
        imageURL:PropTypes.string,
        price: PropTypes.number,
        shops: PropTypes.arrayOf(PropTypes.number),
        category: PropTypes.string,
        type: PropTypes.string,
        description: PropTypes.string,
        details: PropTypes.string,
        material: PropTypes.string,
        care: PropTypes.string,
        size: PropTypes.shape({
            width: PropTypes.number,
            height: PropTypes.number
        }),
    })),
    activeRoute: PropTypes.string
}