import React  from 'react';
import PropTypes from 'prop-types';

// styles
import '../../styles/Product/Category.scss';

// components
import CategoryCard from './CategoryCard';

const Category = ({ categories }) => {
    return (
        <div className="Category">
            <div className="Category__title-container">
                <h4 className="Category__title">Productos</h4>
            </div>
            <div className="Category__list" >
            {categories.map((category) => {
                return(
                    <CategoryCard key={ category.name } category={ category } />
                )
            })}
            </div>
        </div>
    )
}

export default Category;

Category.propTypes = {
    categories: PropTypes.arrayOf(PropTypes.shape({
        name: PropTypes.string,
        imageURL: PropTypes.string
    }))
}
