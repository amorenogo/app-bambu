import React, { useContext, useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';

// styles
import '../../styles/Product/ProductDescription.scss';

// context
import { ProductsContext, StoresContext, FavProductsContext } from '../../context';

// icons
import FavoriteIcon from '@material-ui/icons/Favorite';
import FavoriteBorderIcon from '@material-ui/icons/FavoriteBorder';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import StoreIcon from '@material-ui/icons/Store';
import EuroIcon from '@material-ui/icons/Euro';

const ProductDescription = ({ match }) => {
    const history = useHistory();
    const [products] = useContext(ProductsContext);
    const [stores] = useContext(StoresContext);
    const [favProducts, setFavProducts] = useContext(FavProductsContext);
    const [product, setProduct] = useState();
    const [store, setStore] = useState();
    
    const isFav = Boolean(favProducts.find((favProduct) => favProduct.id == match.params.id));

    useEffect(() => {
        setProduct(products.find((product) => product.id == match.params.id));
    }, [])

    useEffect(() => {
        const selectedProduct = product;
        stores.find((store) => {
            if(selectedProduct && store.id === selectedProduct.shops[0]) {
                setStore(stores.find((store) => store.id === product.shops[0]));
            }
        })
    }, [product])

    const toggleFav = () => {
        if(!isFav) {               
            setFavProducts([
                ...favProducts, 
                product
            ]);
        } else {
            const filteredFav = favProducts.filter((fav) => fav.id !== product.id)
            setFavProducts(filteredFav);
        }
    }

    return (
        <div className="ProductDescription">
            <div className="ProductDescription__header">
                <ArrowBackIosIcon className="Arrow" onClick={ history.goBack }/>
                <h4 className="ProductDescription__title">Descripción</h4>
            </div>
            {product && store ? (
                <div className="ProductDescription__info">
                    <img className="ProductDescription__img" src={ product.imageURL } alt={ product.name }/>
                    <div className="ProductDescription__bottom">
                        <p className="ProductDescription__type" >{ product.type.split("-").join(" ") }</p>
                        <div className="ProductDescription__summary">
                            <div className="ProductDescription__store-container">
                                <StoreIcon className="ProductDescription__store-icon"/>
                                <p className="ProductDescription__store-name">{ store.name }</p>
                            </div>
                            <div className="ProductDescription__price-container">
                                <EuroIcon className="ProductDescription__price-icon"/>
                                <p className="ProductDescription__price">{ product.price }</p>
                            </div>
                        </div>
                        {isFav ? <FavoriteIcon className="Favorite" onClick={ toggleFav }/> : <FavoriteBorderIcon className="Favorite--border" onClick={ toggleFav }/>}               
                        <p className="ProductDescription__name">{ product.name }</p>                        
                        <p className="ProductDescription__description">{ product.details }</p>
                    </div>
                </div>            
            ) : null}
        </div>
    )
}

export default ProductDescription;

