import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

// styles
import '../../styles/Product/CategoryCard.scss';

const CategoryCard = ({ category }) => {
    const [routeLink, setRouteLink] = useState('/');

    useEffect(() => {
        switch(category.name) {
            case 'Alimentos':
                setRouteLink('/food');
                break;
            case 'Para Llevar':
                setRouteLink('/takeaway');
                break;
            case 'Higiene':
                setRouteLink('/hygiene');
                break;
            case 'Cosmética':
                setRouteLink('/makeup');
                break;
            default: 
                break;
        }
    }, [])

    return (
        <div className="CategoryCard">
            <Link to={ routeLink }>
                <img src={ category.imageURL } alt={ category.name }/>
            </Link>
        </div>
    )
}

export default CategoryCard;

CategoryCard.propTypes = {
    category: PropTypes.object
}
