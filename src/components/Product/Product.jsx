import React, { useEffect, useState, useContext } from 'react';
import { useLocation, Switch, Route } from 'react-router-dom';

// styles
import '../../styles/Product/Product.scss';

// context
import { ProductsContext } from '../../context';

// components
import ProductsList from './ProductsList';
import ProductDescription from './ProductDescription';

const Product = () => {
    const location = useLocation();
    const [products] = useContext(ProductsContext);
    const [filteredProducts, setFilteredProducts] = useState([]);
    const [activeRoute, setActiveRoute] = useState([]);

    useEffect(() => {
        const activeRoute = location.pathname.slice(1);
        switch(activeRoute) {
            case 'food':
                setFilteredProducts(products.filter((product) => product.category === 'alimentacion'));
                break;
            case 'takeaway':
                setFilteredProducts(products.filter((product) => product.category === 'para-llevar'));
                break;
            case 'hygiene':
                setFilteredProducts(products.filter((product) => product.category === 'higiene'));
                break;
            case 'makeup':
                setFilteredProducts(products.filter((product) => product.category === 'cosmetica'));
                break;
            default: 
                break;
        }
        setActiveRoute(activeRoute);
    }, [])    

    return (
        <div className="Food">            
            <Switch>
                <Route exact path="/food" render={() => <ProductsList filteredProducts={ filteredProducts } activeRoute={ activeRoute }/>}/>
                <Route exact path="/hygiene" render={() => <ProductsList filteredProducts={ filteredProducts } activeRoute={ activeRoute }/>}/>
                <Route exact path="/makeup" render={() => <ProductsList filteredProducts={ filteredProducts } activeRoute={ activeRoute }/>}/>
                <Route exact path="/takeaway" render={() => <ProductsList filteredProducts={ filteredProducts } activeRoute={ activeRoute }/>}/>
                
                <Route exact path="/food/:id" component={ ProductDescription }/>
                <Route exact path="/hygiene/:id" component={ ProductDescription }/>
                <Route exact path="/makeup/:id" component={ ProductDescription }/>
                <Route exact path="/takeaway/:id" component={ ProductDescription }/>
                <Route exact path="/product/:id" component={ ProductDescription }/>
            </Switch>            
        </div>
    )
}

export default Product;
