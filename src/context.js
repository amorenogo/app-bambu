import React from 'react';

export const BambuInfoContext = React.createContext([]);
export const UsersContext = React.createContext([]);
export const ActiveUserContext = React.createContext([]);
export const ProductsContext = React.createContext([]);
export const FavProductsContext = React.createContext([]);
export const StoresContext = React.createContext([]);