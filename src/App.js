import React, { useState, useEffect }  from 'react';
import { Route, Switch } from 'react-router-dom';
import { ToastContainer, toast } from 'react-toastify';

// styles
import './App.scss';
import 'react-toastify/dist/ReactToastify.css';

// JSON
import usersData from './data/usersData.json';
import bambuData from './data/bambuData.json';
import productsData from './data/productsData.json';
import storesData from './data/storesData.json';
import categoriesData from './data/categoriesData.json';
import aboutData from './data/aboutData.json';

// context
import { BambuInfoContext, UsersContext, ActiveUserContext, ProductsContext, FavProductsContext, StoresContext } from './context';

// components
import StoresMap from './components/StoresMap';
import Navbar from './components/Navbar';
import Footer from './components/Footer';
import About from './components/About';
import Home from './components/Home';
import LaunchScreen from './components/LaunchScreen';
import Access from './components/Access/Access';
import Category from './components/Product/Category';
import Product from './components/Product/Product';
import Profile from './components/Profile/Profile';


const App = () => {
  const [users, setUsers] =  useState(usersData);
  const [bambuInfo, setBambuInfo] = useState(bambuData);
  const [categories, setCategories] = useState(categoriesData);
  const [products, setProducts] = useState(productsData);
  const [favProducts, setFavProducts] = useState([]);
  const [stores, setStores] = useState(storesData);
  const [activeUser, setActiveUser] = useState();
  const [about, setAbout] = useState(aboutData);
  
  useEffect(() => {
    sessionStorage.setItem('usersData', JSON.stringify(users));    
  }, [users])

  useEffect(() => {
    sessionStorage.setItem('activeUser', JSON.stringify(activeUser));    
  }, [activeUser])

  return (
    <div className="App">
      <UsersContext.Provider value={[users, setUsers]}>        
        <ActiveUserContext.Provider value={[activeUser, setActiveUser]}>
          <Navbar products={products}/>
          <BambuInfoContext.Provider value={[bambuInfo]}>
            <ProductsContext.Provider value={[products]}>
              <FavProductsContext.Provider value={[favProducts, setFavProducts]}>
                <StoresContext.Provider value={[stores]}>              
                  <Switch>
                    <Route exact path="/" component={ LaunchScreen }/>
                    <Route exact path="/home" component={ Home } />
                    <Route exact path="/access" component={ Access } />
                    <Route exact path="/categories" render={() => <Category categories={ categories }/> }/>
                    <Route exact path="/stores" component={ StoresMap }/>
                    <Route path="/food" component={ Product } />
                    <Route path="/hygiene" render={() => <Product /> }/>
                    <Route path="/makeup" render={() => <Product /> }/>
                    <Route path="/takeaway" render={() => <Product /> }/>
                    <Route path="/product" render={() => <Product /> }/>
                    <Route path="/profile" render={() => <Profile /> }/>
                    <Route path="/about" render={() => <About about={ about }/> }/>
                  </Switch>
                </StoresContext.Provider>
              </FavProductsContext.Provider>
            </ProductsContext.Provider>
          </BambuInfoContext.Provider>
          <Footer />
          <ToastContainer position="bottom-right" />
        </ActiveUserContext.Provider>
      </UsersContext.Provider>
    </div>
  );
}

export default App;
